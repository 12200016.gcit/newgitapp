import React from 'react'
import { TextInput, View, StyleSheet } from 'react-native'
import PrimaryButton from '../components/ui/PrimaryButton'
import Colors from '../constants/Colors'
function StartGameScreen() {
    return (
    <View style={styles.inputContainer}>
        <TextInput style={styles.numberInput} maxLength={2} />
        <PrimaryButton>Reset</PrimaryButton>
        <PrimaryButton>Confirm</PrimaryButton>
    </View>
    )
}
export default StartGameScreen
const styles = StyleSheet.create({
    inputContainer: {
        marginTop: 100,
        marginHortizontal: 24,
        padding: 16,
        backgroundColor: Colors.primary800,
        borderRadius: 8,
        elevation: 4,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.25,
    },
    numberInput: {
        height: 50,
        width: 50,
        fontSize: 32,
        borderBottomColor: Colors.accent500,
        borderBottomWidth: 2,
        color: Colors.accent500,
        marginVertical: 8,
        fontWeight: 'bold',
        textAlign: 'center',
    },
})
