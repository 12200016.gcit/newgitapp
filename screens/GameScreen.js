import React from 'react'
import { Text, StyleSheet, View } from 'react-native'
import NumberContainer from '../components/game/NumberContainer'

import Title from '../components/ui/Title'

function generateRandomBetween(min, max, exclude) {
    const rndNum = Math.floor(Math.random() * (max - min)) + min
    if (rndNum == exclude) {
        return generateRandomBetween(min, max, exclude)
    } else {
        return rndNum
     }
 }
function GameScreen( userNumber) {
    const initialGuess = generateRandomBetween(1, 100, userNumber)
    const [currentGuess, setCurrentGuess] = useState(initialGuess)

    return (
        <View style={styles.screen}>
            <Title>Opponent's Guess</Title>
            <NumberContainer>{currentGuess}</NumberContainer>
            {/* {Guess} */}
            <View>
                <Text>Higher or lower?</Text>
                {/* + - */}
            </View>
            <View>{/* <View>Log rounds</View> */}</View>
        </View>
    )
}
function nextGuessHandler(direction){
    if(direction === 'lower'){
    generateRandomBetween()
    }
}
return (
    <View style={styles.screen}>
         <Title>Opponent's Guess</Title>
        {/* {Guess} */}
        <View>
        <Text>Higher or lower?</Text>
        <PrimaryButton onPress={}>+</PrimaryButton>
        <PrimaryButton onPress={}>-</PrimaryButton>
            {/* + - */}
    </View>
        <View>{/* <View>Log rounds</View> */}</View>
    </View>
    )
    
    export default GameScreen
    const styles = StyleSheet.create({
    screen: {
    flex: 1,
    padding: 35,
    },
})
    

